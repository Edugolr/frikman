import 'primevue/resources/themes/saga-blue/theme.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { plugin, defaultConfig } from '@formkit/vue'
import '@/assets/css/variables.css'
import App from './App.vue'
import router from './router'
import { VueFire, VueFireAuth } from 'vuefire'
import vClickOutside from 'click-outside-vue3'
import { firebaseApp } from './firebase'
import PrimeVue from 'primevue/config'

defaultConfig({
  theme: 'genesis' // will load from CDN and inject into document head
})
const app = createApp(App)

app.use(VueFire, {
  // imported above but could also just be created here
  firebaseApp,
  modules: [
    // we will see other modules later on
    VueFireAuth()
  ]
})
app.use(PrimeVue)
app.use(plugin, defaultConfig)
app.use(createPinia())
app.use(router)
app.use(vClickOutside)
app.mount('#app')
