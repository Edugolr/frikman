import { initializeApp } from 'firebase/app'
import { getStorage } from 'firebase/storage'
import { getFirestore, collection } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyDCAgbMRiKZlM1xPqyUxZX3Ezr5E_SrKAY',
  authDomain: 'familjenwikman-7edcb.firebaseapp.com',
  projectId: 'familjenwikman-7edcb',
  storageBucket: 'familjenwikman-7edcb.appspot.com',
  messagingSenderId: '92218778344',
  appId: '1:92218778344:web:85d8c44d2e3764a48e9f5d',
  measurementId: 'G-BEEK29B0GF'
}

// Initialize Firebase
export const firebaseApp = initializeApp(firebaseConfig)
export const database = getFirestore(firebaseApp)
export const auth = getAuth()

const storage = getStorage(firebaseApp)

// here we can export reusable database references
export const todosRef = collection(database, 'osa')
