const TypeChecks = {
  isString: (value: any): value is string => typeof value === 'string',
  isHTMLElement: (value: any): value is HTMLElement => value instanceof HTMLElement,
  isFunction: (value: any): value is Function => typeof value === 'function',
  isObject: (value: any): value is object => typeof value === 'object',
  isNumber: (value: any): value is number => typeof value === 'number',
  isBoolean: (value: any): value is boolean => typeof value === 'boolean',
  isNull: (value: any): value is null => value === null,
  isUndefined: (value: any): value is undefined => typeof value === 'undefined',
  isArray: (value: any): value is Array<any> => Array.isArray(value),
  isDate: (value: any): value is Date => value instanceof Date
}

export default TypeChecks
