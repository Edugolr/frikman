import {
  getStorage,
  ref as storageRef,
  getDownloadURL,
  uploadBytesResumable,
  listAll,
  getMetadata
} from 'firebase/storage'

const storage = getStorage()

const getUrl = async (ref: any) => {
  return await getDownloadURL(ref)
}

export const getProfileImage = async (imageName: string) => {
  const imageRef = storageRef(storage, `profileImages/${imageName}`)
  return await getUrl(imageRef)
}

export const getGalleryThumbnailImages = async () => {
  const galleryRef = storageRef(storage, `gallery/thumbnails`)
  const images = await listAll(galleryRef)
  const urls = await Promise.all(
    images.items.map(async (image) => {
      return await getUrl(image)
    })
  )
  return urls
}

export const getGalleryImages = async () => {
  const galleryRef = storageRef(storage, `gallery`)
  const images = await listAll(galleryRef)
  const urls = await Promise.all(
    images.items.map(async (image) => {
      return await getUrl(image)
    })
  )
  return urls
}

export const getGalleryImageFromThumbnail = async (thumbNailUrl: string) => {
  const imageName = thumbNailUrl.split('thumbnails%2F')[1].split('_')[0]

  const galleryRef = storageRef(storage, `gallery/${imageName}`)
  return await getUrl(galleryRef)
}

export const uploadSurpriseImage = async (file: any) => {
  const fileName = file.name || Date.now()
  const imageRef = storageRef(storage, `surpriseImage/${fileName}`)
  const uploadTask = uploadBytesResumable(imageRef, file)

  uploadTask.on(
    'state_changed',
    (snapshot) => {
      const progress = (Number(snapshot.bytesTransferred) / Number(snapshot.totalBytes)) * 100
      console.log('Upload is ' + progress + '% done')
    },
    (error) => {
      console.log(error)
    },
    () => {
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        console.log('File available at', downloadURL)
      })
    }
  )
}

export const uploadGalleryImage = async (file: any) => {
  const fileName = file.name || Date.now()
  const imageRef = storageRef(storage, `gallery/${fileName}`)
  const uploadTask = uploadBytesResumable(imageRef, file)

  uploadTask.on(
    'state_changed',
    (snapshot) => {
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      const progress = (Number(snapshot.bytesTransferred) / Number(snapshot.totalBytes)) * 100
      console.log('Upload is ' + progress + '% done')
      switch (snapshot.state) {
        case 'paused':
          console.log('Upload is paused')
          break
        case 'running':
          console.log('Upload is running')
          break
      }
    },
    (error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
        case 'storage/unauthorized':
          // User doesn't have permission to access the object
          break
        case 'storage/canceled':
          // User canceled the upload
          break

        // ...

        case 'storage/unknown':
          // Unknown error occurred, inspect error.serverResponse
          break
      }
    },
    () => {
      // Upload completed successfully, now we can get the download URL
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        console.log('File available at', downloadURL)
      })
    }
  )
}
