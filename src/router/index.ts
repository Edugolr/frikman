import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import GalleryView from '@/views/GalleryView.vue'
import { getCurrentUser } from 'vuefire'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Hem',
      component: GalleryView,
      meta: {
        keepAlive: true
      }
    },
    // {
    //   path: '/location',
    //   name: 'Plats',
    //   component: () => import('@/views/LocationView.vue'),
    //   meta: {
    //     keepAlive: true,
    //     requiresAuth: true
    //   }
    // },
    // {
    //   path: '/clothes',
    //   name: 'Kläder',
    //   component: () => import('@/views/ClothesView.vue'),
    //   meta: {
    //     keepAlive: true,
    //   }
    // },
    // {
    //   path: '/schedule',
    //   name: 'Schema',
    //   component: () => import('@/views/ScheduleView.vue'),
    //   meta: {
    //     keepAlive: true,
    //   }
    // },
    // {
    //   path: '/menu',
    //   name: 'Meny',
    //   component: () => import('@/views/MenuView.vue'),
    //   meta: {
    //     keepAlive: true,
    //   }
    // },
    // {
    //   path: '/barMenu',
    //   name: 'Baren',
    //   component: () => import('@/views/BarMenuView.vue'),
    //   meta: {
    //     keepAlive: true,
    //   }
    // },

    // {
    //   path: '/entourage',
    //   name: 'Bröllopsfölje',
    //   component: () => import('@/views/WeddingEntourage.vue'),
    //   meta: {
    //     keepAlive: true,
    //   }
    // },

    {
      path: '/gallery',
      name: 'Bröllops bilder',
      component: GalleryView,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/practical',
      name: 'Praktisk kunskap',
      component: () => import('@/views/PracticalKnowledgeView.vue'),
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/economi',
      name: 'Ekonomi',
      component: () => import('@/views/SharedEconomy.vue'),
      meta: {
        keepAlive: true,
        requiresAuth: true
      }
    },
    {
      path: '/quiz',
      name: 'Quiz Pari & Christofer',
      component: () => import('@/views/QuizPari.vue'),
      meta: {
        keepAlive: true,
      }
    },
    {
      path: '/quizeve',
      name: 'Quiz Eve & Robin',
      component: () => import('@/views/QuizEve.vue'),
      meta: {
        keepAlive: true
      }
    }
  ]
})

router.beforeEach(async (to) => {
  // routes with `meta: { requiresAuth: true }` will check for
  // the users, others won't
  if (to.meta.requiresAuth) {
    const currentUser = await getCurrentUser()

    const allowedUsersEmails = ['christofer.wikman@gmail.com', 'pariwikmans@gmail.com']
    if (!currentUser || !currentUser.email || !allowedUsersEmails.includes(currentUser.email)) {
      return {
        path: '/'
      }
    }
  }
})

export default router
