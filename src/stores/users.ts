import { defineStore } from 'pinia'
import { database } from '@/firebase'
import { doc } from 'firebase/firestore'
import { useDocument } from 'vuefire'
import { collection } from 'firebase/firestore'

export const useUserStore = defineStore('Users', () => {
  const pari = (useDocument(doc(collection(database, 'users'), 'Pari')) as any) || null
  const christofer = (useDocument(doc(collection(database, 'users'), 'Christofer')) as any) || null

  return { pari, christofer }
})
