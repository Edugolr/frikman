import { computed } from 'vue'
import { defineStore } from 'pinia'
import { database } from '@/firebase'
import { collection } from 'firebase/firestore'
import { useCollection } from 'vuefire'

export const useOSAStore = defineStore('OSA', () => {
  const allOSA = useCollection(collection(database, 'osa'))

  const guestsComing = computed(() => {
    return allOSA.value.filter((osa: any) => osa.participating !== 'inget').length
  })
  return { guestsComing }
})
