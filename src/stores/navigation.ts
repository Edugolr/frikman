import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useNavigationStore = defineStore('navigation', () => {
  const isNavigationDrawerOpen = ref(false)

  function toggleNavigationDrawer() {
    isNavigationDrawerOpen.value = !isNavigationDrawerOpen.value
  }
  function openNavigationDrawer() {
    isNavigationDrawerOpen.value = true
  }
  function closeNavigationDrawer() {
    isNavigationDrawerOpen.value = false
  }
  return {
    isNavigationDrawerOpen,
    toggleNavigationDrawer,
    openNavigationDrawer,
    closeNavigationDrawer
  }
})
